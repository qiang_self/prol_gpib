# README #

A very minimalist Python library for Prologix USB-GPIB adapter.

### What is this repository for? ###

To access equipment on GPIB through Prologix USB-GPIB adapter with Python. The adapter shows up on USB as a serial port. Following some simple sequences, communication between nodes on GPIB and the host can be established. Here are some minimalist functions written in Python that can be used for this purpose.