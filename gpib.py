# -*- coding: utf-8 -*-
"""
Communicating with test instruments through Prologix GPIB
Created on: 1:40 PM, 9/26/2016
Last edited on: Tue, Oct 11, 2016  4:41:17 PM
@author: QLin
Ver 1.2
Copyright 2016 by Qiang Lin
"""
import serial
import sys
import time

def main():
# Examples of usages. This main function is for testing purpose.
# Use import gpib to your own codes.
# run gpib.py <COM port to Prologix>
    gp = Gpib(sys.argv[1])
# Output: COM38: Prologix GPIB-USB Controller version 6.101
# scan for instruments on the bus from address 1 to 30.
    gp.scan(silence=False)

# Output:
# Address 1:      nothing found.
# Address 2:      nothing found.
# Address 3:      nothing found.
# Address 4:      nothing found.
# Address 5:      nothing found.
# Address 6:      nothing found.
# Address 7:      Rigol Technologies,DM3058,DM3L153500155,01.01.00.02.01.00
# Address 8:      nothing found.
# Address 9:      nothing found.
# Address 10:     nothing found.
# Address 11:     nothing found.
# Address 12:     nothing found.
# Address 13:     nothing found.
# Address 14:     nothing found.
# Address 15:     nothing found.
# Address 16:     nothing found.
# Address 17:     nothing found.
# Address 18:     nothing found.
# Address 19:     nothing found.
# Address 20:     nothing found.
# Address 21:     nothing found.
# Address 22:     nothing found.
# Address 23:     nothing found.
# Address 24:     nothing found.
# Address 25:     nothing found.
# Address 26:     nothing found.
# Address 27:     nothing found.
# Address 28:     nothing found.
# Address 29:     nothing found.
# Address 30:     nothing found.

# Reset instrument at address 7.
    gp.command(7,'*RST')
    time.sleep(5)

# Query device ID at address 7.
    print(gp.query(7,'*IDN?'))
# Output: Rigol Technologies,DM3058,DM3L153500155,01.01.00.02.01.00

# One exit, this message is shown: GPIB adaptor has been reset and COM port has been closed.
    return

class Gpib():
    port = None
    opened = False
    eq_list = []

    def __init__(self,port_number):
        if (self.port == None) or (not self.port.isOpen()):
            self.opened = self.init_gpib_card(port_number)
        return

    def scan(self,silence=True):
        self.eq_list = self.scan_eq(self.port,silence=silence)
        return

    def lock(self,eq_addr):
        self.port.write(('++addr {}\n'.format(eq_addr)).encode())
        self.port.write('++llo\n'.encode())
        return

    def reset(self):
        self.port.write('++rst\n'.encode())
        time.sleep(3)
        self.init_cfg()
        return

    def init_cfg(self):
        self.port.write('++savecfg 0\n'.encode())
        # 3 methods: LF only, EOI only and LF+EOI.
        self.port.write('++eos 2\n'.encode())  # LF+EOI, some Anritsu instruments require this.
        #self.port.write('++eos 3\n'.encode()) # None (EOI only), hardware only per standard.
        self.port.write('++mode 1\n'.encode())
        self.port.write('++eoi 1\n'.encode())
        self.port.write('++eot_char 13\n'.encode())
        self.port.write('++eot_enable 1\n'.encode())
        self.port.write('++auto 0\n'.encode())
        self.port.write('++read_tmo_ms 300\n'.encode())
        return

    def find_model_gpib(self,eq_addr):
        model = ''
        eq_info = self.query(eq_addr,'*IDN?')
        if (eq_info != ''):
            model = eq_info.split(',')[1].strip()
        return model

    def command(self,eq_addr,cmd,delay=0.0):
        self.port.write(('++addr {}\n'.format(eq_addr)).encode())
        self.port.write((cmd+'\n').encode())
        if (delay != 0.0):
            time.sleep(delay)
        return

    def query(self,eq_addr,cmd,delay=0.1):
        self.command(eq_addr,cmd)
        time.sleep(delay)
        self.port.write('++read eoi\n'.encode())
        ans = (self.port.readline().decode()).strip()
        return ans


    def init_gpib_card(self,ser_number):
        try:
            self.port = serial.Serial('COM' + '{}'.format(ser_number), 9600, timeout=0.5)
            self.port.baudrate = self.port.BAUDRATES[-1]
        except serial.SerialException:
            print('Cannot open serial port of GPIB controller.')
            return False

        self.port.write('++ver\n'.encode())
        ans = self.port.readline().decode()

        if 'Prologix GPIB-USB Controller' in ans:
            self.init_cfg()
            print('COM{0}: {1}'.format(ser_number,ans))
        else:
            print('COM{} is NOT GPIB interface. Please check port number.'.format(ser_number))
            return False
        return True

    def gpib_release(self,eq_addr):
        self.command(eq_addr,'++loc')
        return

    def port_close(self):
        self.port.close()
        return


    def scan_eq(self,gpib_ser,silence=False):
        eq_list = []
        for eq_addr in range(1,31):
            id_in = self.query(eq_addr,'*IDN?',delay=0.1)
            if id_in != '':
                if not silence:
                    print('Address {}:\t'.format(eq_addr) + id_in)
                eq_list.append((eq_addr,id_in))
                self.gpib_release(eq_addr)
            else:
                if not silence:
                    print('Address {}: \tnothing found.'.format(eq_addr))

        return eq_list

    def add(self,eq_addr):
        id_in = self.query(eq_addr, '*IDN?')
        if id_in != '':
            print('Address {}:\t'.format(eq_addr) + id_in)
            self.eq_list.append((eq_addr, id_in))
            status = True
        else:
            status = False
        return status,id_in

    def __del__(self):
        self.port.write('++rst\n'.encode())
        self.port.close()
        print('GPIB adaptor has been reset and COM port has been closed.')
        self.opened = False
        return

#########################################
if __name__ == '__main__':
    main()
#########################################
